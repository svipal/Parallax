{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -Wno-missing-safe-haskell-mode #-}
module Paths_Parallax (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/mistigri/Projects/Parallax/.stack-work/install/x86_64-linux-tinfo6/391727167505a9f5ed86ff9401f1416085863cc27ab79746600bb27a1121cdcf/9.0.1/bin"
libdir     = "/home/mistigri/Projects/Parallax/.stack-work/install/x86_64-linux-tinfo6/391727167505a9f5ed86ff9401f1416085863cc27ab79746600bb27a1121cdcf/9.0.1/lib/x86_64-linux-ghc-9.0.1/Parallax-0.1.0.0-AeshAJNLBVL6AkLNZhXTR"
dynlibdir  = "/home/mistigri/Projects/Parallax/.stack-work/install/x86_64-linux-tinfo6/391727167505a9f5ed86ff9401f1416085863cc27ab79746600bb27a1121cdcf/9.0.1/lib/x86_64-linux-ghc-9.0.1"
datadir    = "/home/mistigri/Projects/Parallax/.stack-work/install/x86_64-linux-tinfo6/391727167505a9f5ed86ff9401f1416085863cc27ab79746600bb27a1121cdcf/9.0.1/share/x86_64-linux-ghc-9.0.1/Parallax-0.1.0.0"
libexecdir = "/home/mistigri/Projects/Parallax/.stack-work/install/x86_64-linux-tinfo6/391727167505a9f5ed86ff9401f1416085863cc27ab79746600bb27a1121cdcf/9.0.1/libexec/x86_64-linux-ghc-9.0.1/Parallax-0.1.0.0"
sysconfdir = "/home/mistigri/Projects/Parallax/.stack-work/install/x86_64-linux-tinfo6/391727167505a9f5ed86ff9401f1416085863cc27ab79746600bb27a1121cdcf/9.0.1/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "Parallax_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "Parallax_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "Parallax_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "Parallax_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "Parallax_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "Parallax_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
