module Tools where

import Data.IORef
import Control.Monad
import qualified Data.HashTable.IO as H
import qualified Data.HashMap.Strict as DHS

class Monad m => MaybeToM m where


  -- if there is nothing we supply a default value.
  (>?>=) :: Maybe t -> t -> (t -> m b) -> m b
  (>?>=) x or f = case x of
    Just y -> do
      f y
    Nothing -> f or

  -- if there is nothing we run an alternate computation
  (>?>/) :: Maybe t -> m b -> (t -> m b) -> m b
  (>?>/) x or f = case x of
    Just y -> do
      f y
    Nothing -> or

  -- same as above but a value to return in the monad instead of a monadic computation
  (>?>/.) :: Maybe a -> b -> (a -> m b) -> m b
  (>?>/.) x or f = (>?>/) x  (return or) f


  -- if there is nothing we do nothing
  (>?>) :: Maybe a -> (a -> m ()) -> m ()
  (>?>) x f = case x of
    Just y -> do
      f y
    Nothing -> return ()

  -- if there is nothing we return Nothing, otherwise we run the computation and wrap the result in maybe
  (>?>>) :: Maybe a -> (a -> m b) -> m (Maybe b)
  (>?>>) x f = case x of 
    Just x -> do 
      f x >>= (return.Just)
    Nothing  -> return Nothing




class (MonadPlus m, MaybeToM m) =>  MaybeToMPlus m where
  -- this might be useful if you already have a mzero value in a monad serving as a default
  (>?>==) ::  Maybe a -> (a -> m b) -> m b
  a >?>== f = case a of
    Nothing -> mzero >>= f
    Just x  -> return x >>= f

instance MaybeToM IO 
instance MaybeToM Maybe 
instance MaybeToM [] 
instance MaybeToMPlus [] 


-- safe list lookup

infix 9 !!?
(!!?) :: [a] -> Int -> Maybe a
(!!?) xs i
    | i < 0     = Nothing
    | otherwise = go i xs
  where
    go :: Int -> [a] -> Maybe a
    go 0 (x:_)  = Just x
    go j (_:ys) = go (j - 1) ys
    go _ []     = Nothing
{-# INLINE (!!?) #-}

-- only do monadic computation if another is true


(=:=) ref value = writeIORef ref value

forMFlag :: (Foldable t1, Monad m) => m Bool -> t1 t2 -> (t2 -> m ()) -> m ()
forMFlag check data' compute = do
  forM_ data' $ \datum -> do
    flag <- check
    when flag (compute datum)
    return ()



-- i do not care
unWrap (Just x ) = x

-- 
lwrap x = [x]
{-# INLINE (&) #-}
(&) x f = f x


addBinding bindings x y = H.mutate bindings x (addBinding')
  where
    addBinding' Nothing = (Just $ [y],())
    addBinding' (Just x) = (Just $ y:x,())
    
getBindings a b = do
  x <- H.lookup a b
  x >?>= [] $ return