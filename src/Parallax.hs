module Parallax where


import GHC.Generics

-- I 
import Unsafe.Coerce


-- i dont want to suffer 
import Data.Maybe

import Control.Monad.IO.Class
import System.IO
import System.Environment
import Data.IORef
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Control.Concurrent
import Control.Monad
import Control.Lens.Combinators


import Device.Nintendo.Switch as Switch

import SDL.Input.Keyboard.Codes
import SDL.Input.Joystick as Joy
import SDL.Input.Keyboard as Keyb
import SDL.Init
import SDL.Event
import SDL.Raw.Event (flushEvents)

-- midi

import Sound.RtMidi as Midi
import Sound.RtMidi.Report


-- conversion etc

import Foreign.C.Types
import Data.Int
import Text.Read
import qualified Data.ByteString as B

-- containers

import qualified Data.Text as T
import qualified Data.IntMap as IMap
import qualified Data.Vector as V
import qualified Data.Vector.Storable as VS
import qualified Data.HashTable.IO as H
import qualified Data.HashMap.Strict as DHS

-- bit fun  

import Data.Bits


--save/load
-- import Data.Serialize
import Data.Aeson 
        (encode
        ,encodeFile
        ,decode
        ,decodeFileStrict
        ,ToJSON
        ,FromJSON)

-- parsing
import Options.Applicative

import Tools
import Midi
import Types


class ( Generic (map)
      , Generic (Frozen map )
      , ToJSON (Frozen map )
      , FromJSON (Frozen map ) ) => Freezable map  where
  type Frozen map :: * 
  freeze :: map  -> IO (Frozen map)
  thaw   :: (Frozen map) -> IO map 


class Freezable map => Mappable map  where
  type MEnv map :: *
  type MEvent map :: *
  mapType :: Int -> Maybe String
  mapType n = mapTypes @map !!? n
  mapTypes :: [String]
  getMappingsFuncs  :: (MEnv map) -> map -> IO [(MEvent map) -> ParallaxM ()]
  getAssignFuncs    :: (MEnv map) -> map -> IO [(MEnv map) -> map -> IO ()]
  startAssigning    :: (MEnv map) -> map -> IO ()



class (MonadIO m, Show mdreport , Show wdreport, Mappable mappings ) =>  ParallaxFam initSettings mdreport wdreport coretype  mappings m | m -> coretype, coretype -> wdreport mdreport initSettings mappings where

  getCore         :: m coretype
  modCore         :: (coretype -> IO coretype) -> m()  
  setCore         :: coretype -> m()
  constructInit   :: [String] -> m initSettings
  cleanParallax   :: m () 
  initParallax    :: initSettings -> m ()

  getApi           :: m Api
  getOutputs       :: m [OutputDevice]
  getInputs        :: m [InputDevice]

  getConsole      :: m (Maybe Console)
  getJoysticks    :: m (V.Vector PxJoy)


  getInterval     :: String -> m Interval
  getPitch        :: String -> m Double
  
  getMappings     :: m mappings

  setMappings     :: m mappings

  configMappings :: m ()
  configMappings = do
    m <- getMappings
    env <- getCore
    liftIO $ do
      let inputLoop = do  
            i <- newIORef 1
            putStrLn "0 : quit"
            forM_ (mapTypes @mappings)$  \mapType ->  do
              ix <- readIORef i
              putStr  $ (show ix) ++ " : map " ++ (show $ mapType) ++ " mappings"
              writeIORef i (ix+1)
            command <- getLine
            let commandNum = readMaybe @Int command
            commandNum >?>/ (do {putStrLn "invalid command"; inputLoop}) $ \num -> do
              case num of 
                0 -> return ()
                n -> do
                  fs <- getAssignFuncs (unsafeCoerce @(MEnv mappings) env)  m
                  (fs !! num) env m
                  inputLoop
      inputLoop
      putStrLn "Configuration finished..."
 
  
  -- we need to do this for concurrency

  midiDaemon :: m (coretype -> IO mdreport)
  watchDaemon :: m (coretype -> IO wdreport)
  config   :: m ()

  start :: m () 
  start = do
  settings <- constructInit =<< (liftIO getArgs)
  initParallax settings
  config
  pr <- getCore
  wd <- watchDaemon 
  md <- midiDaemon 
  let loop = do
        whyEnd <- race (wd pr) (md pr) 
                          
        print  whyEnd
        putStrLn "do you want to start over ?"
        getLine >>= \case 
          "y" -> loop
          _ -> return ()
  liftIO loop
  cleanParallax

instance MaybeToM ParallaxM

data ParallaxM a = PM { runPR :: Parallax -> IO (a,Parallax) } deriving Generic

-- newtype OrleGame  a = PM { runPR :: Core -> IO (a, Core)}

instance Functor ParallaxM where
  fmap f pr@(PM g) = PM $! \env -> do
    (val,env') <- g env
    return (f val, env')

instance Applicative ParallaxM where
  pure g = PM $! \env -> do
    return (g,env)
  pr <*> pr2 = PM $! \env -> do
    (g2b, env') <- runPR pr env
    (g2, env'') <- runPR pr2 env'
    return (g2b g2, env'')

instance Monad ParallaxM where
  return = pure
  pr >>= f = PM $! \env -> do
    (g,env') <- runPR pr env
    let pr2 = f g
    (g2,env'') <- runPR pr2 env'
    return (g2,env'')
  {-# INLINABLE return #-}   

instance  MonadIO  ParallaxM where
  liftIO action = PM $ \env -> do
    a <- action
    return (a,env)
  {-# INLINE liftIO #-}   



