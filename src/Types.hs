module Types where 



import GHC.Generics
import Control.Concurrent.Async
import Control.Concurrent.MVar


-- i dont want to suffer 
import System.IO
import Data.IORef

import Foreign.C.Types
import Data.Int
import Text.Read
import qualified Data.ByteString as B

-- containers

import qualified Data.Text as T
import qualified Data.IntMap as IMap
import qualified Data.Vector as V
import qualified Data.Vector.Storable as VS
import qualified Data.HashTable.IO as H
import qualified Data.HashMap.Strict as DHS

-- import Data.Serialize
import Data.Aeson


import Device.Nintendo.Switch as Switch

import SDL.Input.Keyboard.Codes
import SDL.Input.Joystick as Joy
import SDL.Input.Keyboard as Keyb
import SDL.Init
import SDL.Event
import SDL.Raw.Event (flushEvents)

-- midi

import Sound.RtMidi as Midi
import Sound.RtMidi.Report


import Options.Generic
import Options.Applicative
import Data.Semigroup ((<>))


import Midi
import Tools

-- -- error types for error reports for midi and commandline loops 
data CLReport = CLEndRequested
              | ReInitRequested deriving (Show,Generic)
data MLReport = ML deriving (Show,Generic)




data ISettings = IS 
  {   isInputs  :: Int
  ,   isOutputs :: Int
  -- can imagine only using some controller types
  -- enabling or not
}

type HashTable k v = H.CuckooHashTable k v

data PxJoy = PxJoy 
    { buttonCount   :: CInt
    , axisesCount   :: CInt
    , ballCount     :: CInt
    , hatCount      :: CInt
    , joy           :: Joystick
}


mkPxJoy :: JoystickDevice -> IO PxJoy
mkPxJoy jst = do
    j <- openJoystick jst
    bC <- numButtons j
    aC <- numAxes j
    blC <- numBalls j 
    hC <- numHats j
    return $ PxJoy bC aC blC hC j

data Parallax = Parallax 
    { inputs        :: V.Vector InputDevice
    , api           :: Api
    , outputs       :: V.Vector OutputDevice
    , console       :: Maybe Console
    , joysticks     :: V.Vector PxJoy
    , mappings      :: Mappings -- channel, mappings
    , newMappings   :: MVar Mappings
    , sliders       :: HashTable String Int16
    , intervals     :: HashTable String Interval
    , bases         :: HashTable String Double
} deriving Generic


data SlidingScale = SM MidiCC Int Int 
                  | SI String
                  | SP -- pitch bend
                   deriving (Eq,Show,Generic)


instance ToJSON MidiCC
instance FromJSON MidiCC
instance ToJSON SlidingScale
instance FromJSON SlidingScale


data Interval = Pure Int Int | Impure Double deriving (Generic,Eq,Show) 
instance ToJSON Interval
instance FromJSON Interval

(.*.) :: Interval -> Interval -> Interval
(.*.) (Pure x y) (Pure x2 y2 ) = Pure (x*x2) (y*y2)
(.*.) (Pure x y) (Impure z) = Impure $ z*(fromIntegral x/fromIntegral y)
(.*.) (Impure z) (Pure x y) =  Impure $ z*(fromIntegral x/fromIntegral y)
(.*.) (Impure z) (Impure z') =  Impure $ z*z'


data Modifier a 
  = MCycled { mcid :: Int, cycled :: [a] } --
  | NoM a
  deriving (Show, Generic)



instance (ToJSON a) => ToJSON (Modifier a)
instance FromJSON a => FromJSON (Modifier a)

data SpecMIDI = AllNotesOff deriving (Show, Generic)

instance ToJSON SpecMIDI
instance FromJSON SpecMIDI

data Time 
  = TMS Int  -- ms
  | TMPO Int Int {- fraction of one beat -} 
  deriving (Show, Generic)

instance ToJSON Time
instance FromJSON Time



data FireEvent        
  = ONote Bool String         -- On/Off , Note
  | OChirp Bool Int Int       -- On/Off, voice number, initial pitch 
  | OTwee Interval Time Int   -- Interval, time to get there , what voice is Twee'd
  | OTranspose Interval Int   -- Interval,  what voice is transposed
  | OSpec SpecMIDI            -- Special MidiEvent 
  | IFlag String              -- Flag Name
  | ICount Bool String        -- +/-, counter name
  | IChangeMappings Int       -- new Mappings ID
  | ISpecialAction String     -- special actions (own system)
  | Turbo Int Int FireEvent -- delay, how many times
  deriving (Show, Generic)


instance ToJSON FireEvent
instance FromJSON FireEvent


data CCIn 
  = CCThreshold Int Int  FireEvent FireEvent -- threshold in, threshold out (useful for pads for instance)
  | CC2CC   MidiCC -- cc to cc
  | CCAddVar String Double  -- Name of the variable to add to, and multiplier
  deriving (Show, Generic)





instance ToJSON CCIn
instance FromJSON CCIn


data MidiMappings = MM 
  { ccMs :: HashTable Int8 [Modifier CCIn]
    -- thresholds
    -- cc to cc
  ,cc14Ms ::HashTable Int [Modifier CCIn]
}deriving (Generic)

data MidiMappings' = MM'
  { ccMs' :: DHS.HashMap Int8 [Modifier CCIn]
    -- thresholds
    -- cc to cc
  ,cc14Ms' ::DHS.HashMap Int [Modifier CCIn]
}deriving (Generic)
data Mappings = M 
    { jMappings :: JoystickMappings
    -- , sMappings :: SwitchMappings
    -- , gMappings :: GCMappings
    ,mMappings  :: MidiMappings
    -- ,kMappings  :: KBMMappings
} deriving (Generic)

data Mappings' = M' 
  { jMappings' :: JoystickMappings'
      -- , sMappings' :: SwitchMappings
      -- , gMappings' :: GCMappings
  , mMappings'  :: MidiMappings'
      -- ,kMappings'  :: KBMMappings
  } deriving (Generic)


data JoystickMappings = JM 
    { buttonMs :: HashTable (Int,Int) [Modifier FireEvent]
    , axMs     :: HashTable (Int,Int) [Modifier CCIn]
    , blMs     :: HashTable (Int,Int) [Modifier CCIn]
    -- , hatsMs   :: HashTable Int (MidiCC,MidiCC)
} deriving (Generic)


instance ToJSON Mappings' where
  toEncoding = genericToEncoding defaultOptions
instance FromJSON Mappings'

-- frozen version
data JoystickMappings' = JM'
    { buttonMs' :: DHS.HashMap (Int,Int) [Modifier FireEvent]
    , axMs'     :: DHS.HashMap (Int,Int) [Modifier CCIn]
    , blMs'     :: DHS.HashMap (Int,Int) [Modifier CCIn]
    -- , hatsMs   :: HashTable Int (MidiCC,MidiCC)
} deriving (Show, Generic)

instance ToJSON JoystickMappings' where
  toEncoding = genericToEncoding defaultOptions
instance FromJSON JoystickMappings'

instance ToJSON MidiMappings' where
  toEncoding = genericToEncoding defaultOptions
instance FromJSON MidiMappings'


frMMappings :: MidiMappings -> IO MidiMappings'
frMMappings (MM cc cc14) = do
    cc' <- H.toList cc
    cc14' <- H.toList cc14
    return $ MM'
        (DHS.fromList cc')
        (DHS.fromList cc14')


thMMappings :: MidiMappings' -> IO MidiMappings
thMMappings (MM' cc' cc14') = do 
    let conv = H.fromList.DHS.toList 
    cc <- conv cc'
    let conv = H.fromList.DHS.toList 
    cc14 <- conv cc14'
    return $ MM cc cc14


frJMappings :: JoystickMappings -> IO JoystickMappings'
frJMappings (JM b a bl) = do
    b' <- H.toList b
    a' <- H.toList a
    bl' <- H.toList bl
    return $ JM'
        (DHS.fromList b')
        (DHS.fromList a')
        (DHS.fromList bl')

thJMappings :: JoystickMappings' -> IO JoystickMappings
thJMappings (JM' b' a' bl') = do 
    let conv = H.fromList.DHS.toList 
    b <- conv b'
    a <- conv a'
    bl <- conv bl'
    return $ JM b a bl


frMappings m = M' <$> frJMappings (jMappings m)
                  <*> frMMappings (mMappings m)



thMappings m = M <$> thJMappings (jMappings' m)
                  <*> thMMappings (mMappings' m)


-- pbiInfo:: ParserInfo Button
-- pbiInfo = info (pBInterval <**> helper)
--   ( briefDesc <> header "hello - a test for optparse-applicative")

-- pBInterval :: Parser Button
-- pBInterval = Interval   <$> strOption ( short 'i' <> help "id for the interval") 
--                         <*> parseInterval 
--                         <*> switch (short 'r' <> help "allow the interval to be reverted on note off")


-- pPure = Pure <$> option auto (short 'n' <> help "numerator" )  
--              <*> option auto (short 'd' <> help "denominator" )

-- pImpure = Impure <$> option auto (short 'r' <> help "ratio" )  

-- parseInterval = pPure <|> pImpure

-- goParseIntervals = execParserPure defaultPrefs pbiInfo 