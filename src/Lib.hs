module Lib where



-- import GHC.Generics



-- -- i dont want to suffer 
-- import System.IO
-- import Data.IORef
-- import Control.Concurrent.Async
-- import Control.Concurrent.MVar
-- import Control.Concurrent
-- import Control.Monad
-- import Control.Lens.Combinators

-- -- controller input

-- import Device.Nintendo.Switch as Switch

-- import SDL.Input.Keyboard.Codes
-- import SDL.Input.Joystick as Joy
-- import SDL.Input.Keyboard as Keyb
-- import SDL.Init
-- import SDL.Event
-- import SDL.Raw.Event (flushEvents)

-- -- midi

-- import Sound.RtMidi as Midi
-- import Sound.RtMidi.Report


-- -- conversion etc

-- import Foreign.C.Types
-- import Data.Int
-- import Text.Read
-- import qualified Data.ByteString as B

-- -- containers

-- import qualified Data.Text as T
-- import qualified Data.IntMap as IMap
-- import qualified Data.Vector as V
-- import qualified Data.Vector.Storable as VS
-- import qualified Data.HashTable.IO as H
-- import qualified Data.HashMap.Strict as DHS

-- -- bit fun  

-- import Data.Bits


-- --save/load
-- -- import Data.Serialize
-- import Data.Aeson 
--         (encode
--         ,encodeFile
--         ,decode
--         ,decodeFileStrict)

-- -- parsing
-- import Options.Applicative

-- import Tools
-- import Midi
-- import Types





-- initParallax = do
--     SDL.Init.initialize [InitJoystick,InitHaptic,InitGameController]
--     console <- Switch.init
--     apis <- Midi.compiledApis
--     let api = if elem JackApi apis then
--                 JackApi
--             else if elem AlsaApi apis then
--                 AlsaApi
--             else
--                 DummyApi
--     z8
--     joysticks <- Joy.availableJoysticks 

--     let jst =   if length joysticks > 0 then
--                     Just $ V.head joysticks
--                 else Nothing

--     pxJoy <- (jst >?>>  mkPxJoy) 

--     bm <- H.new
--     al <- H.new
--     bl <- H.new
     
--     s <- H.new
--     i <- H.new
--     b <- H.new

--     new <- newEmptyMVar

--     return Parallax {
--         inputs = V.fromList [input],
--         output =  output,
--         api = api,
--         console = Just console,
--         joystick = pxJoy,
--         midiMappings = JM bm al bl,
--         newMappings = new,
--         intervals = i,
--         bases = b,
--         sliders= s
--     } 
    
-- exitParallax p = do
--     V.forM (inputs p) closePort
--     closePort (output p) 
--     (console p) >?> Switch.exit 
--     (joystick p) >?> (closeJoystick.joy)




-- runApp :: Parallax -> IO ()
-- runApp parallax = do
--     configureParallax parallax
--     postConfig parallax
--     race_ (handleConsole parallax)
--           (loopParallax parallax)
--     exitParallax parallax


-- postConfig parallax = do
--   forM (inputs parallax) $ \input -> 
--     openPort input 0 "pxIn"
--   openPort (output parallax ) 0 "pxOut" 

-- handleConsole pr = do 
--   let continue = handleConsole pr
--   let fileName = "parallaxConfig"
--   putStrLn "Midi Transmission Active ! What do you want to do ?"
--   putStrLn "s : save configuration "
--   putStrLn "d : debug "  
--   putStrLn "l : load another configuration file."
--   putStrLn "q : quit the program "        
--   command <- getLine
--   case command of 
--       "s" ->  do 
--           f <- frJMappings (midiMappings pr)
--           encodeFile fileName f
--           continue 
--       "d" -> do
--         putStrLn "---- Current Midi Devices ----"
--         forM (inputs pr) $ \input -> do
--           ok <- ready input 
--           putStrLn $ show input ++ "ready : " ++ show ok
--         ok2 <- ready $ output pr 
--         putStrLn $ show (output pr) ++ "ready : " ++ show ok2
--         putStrLn "---- Current Midi Mappings ----"
--         print =<< frJMappings (midiMappings pr) 
--         continue
--       "l" ->  do 
--           ms' :: Maybe JoystickMappings' <- decodeFileStrict fileName
--           print ms'
--           ms' >?>/ (print "no valid save data") $ \ms -> do
--             putStrLn " file loaded"
--             newM  <- thJMappings ms
--             putMVar (newMappings pr) newM
--             continue
--       "q" -> do
--           putStrLn "ending the program."
--       _ -> do
--           putStrLn "invalid command"
--           continue 

-- configureParallax :: Parallax -> IO ()
-- configureParallax pr = do
--     let mappings = (midiMappings pr) 
--     let bMappings = buttonMs mappings
--     let aMappings = axMs mappings
--     (joystick pr) >?>/ 
--         (putStrLn "no standard joystick available") $ \pxj -> do   
--             (putStrLn.T.unpack.joystickDeviceName.V.head) =<< availableJoysticks
--             let assignButton = do
--                   pollEvents
--                   putStr "press a Button"
--                   let go 0 =  putStrLn ""
--                       go x = do
--                           putStr "."
--                           hFlush stdout
--                           threadDelay 20000
--                           go $ x - 1
--                   go 50
--                   events <- pollEvents    
--                   forM_ events $ \event -> case event of 
--                       Event timestamp (JoyButtonEvent (JoyButtonEventData id button JoyButtonPressed)) -> do
--                           putStrLn $ "What do you want to assign to button " ++ (show button) ++ " on controller " ++ (show id)
--                           putStrLn $ "i : Interval change. -i $1 what interval eg [1 3] or [1.4]" 
--                           putStrLn   "n : Note. $1 : note, [A4] $2 (opt) : velocity id. [i1234]"
--                           putStrLn   "c : CC Toggle. $1 : CC number, [v12] $2 : value when pressed, [^127]. $3 : value when released, [0]"
--                           putStrLn   "p : Panic."
--                           putStrLn   "anything else : Nothing."
--                           getLine >>= \case
--                               'n':' ':rest -> do
--                                 let both = words rest
--                                 let (note, slider) = 
--                                       if length both == 2 then 
--                                         (mkNote (both !! 0), Just $ both !! 1)
--                                       else (Nothing, Nothing)
--                                 note >?>/ (putStrLn "Invalid arguments") $ \note ->
--                                   addBinding bMappings (fromIntegral id, fromIntegral button) (ToggleNote {velId = unWrap slider, note =  note})
--                               ('i':rest) -> do
--                                 let result = goParseIntervals (words rest)
--                                 case result of 
--                                   Success interval -> addBinding bMappings (fromIntegral id, fromIntegral button) interval
--                                   Failure failure -> print failure  
--                                   _ -> print "what"
--                               "p" -> do 
--                                   addBinding bMappings (fromIntegral id, fromIntegral button) $ Panic 
--                                   putStrLn $ "panic assigned to button" ++ (show button)
--                               'c':rest -> do
--                                   let arguments = parseArguments (rest)
--                                   arguments >?>/ (putStrLn "Invalid arguments.") $ \(midiCC, pressed, released) -> do
--                                       addBinding bMappings (fromIntegral id, fromIntegral button) $ HardCC  midiCC pressed released
--                                       print midiCC
--                                       putStrLn $ " assigned to button" ++ (show button)
--                               _ -> putStrLn "..."
--                       _ -> return ()
--             let assignAxis= do
--                   pollEvents
--                   putStr "move an Axis"
--                   let go 0 =  putStrLn ""
--                       go x = do
--                           putStr "."
--                           hFlush stdout
--                           threadDelay 10000
--                           go $ x - 1
--                   go 100
--                   waitAxis <- newIORef True
--                   events <- pollEvents    
--                   forMFlag (readIORef waitAxis) events $ \event -> case event of
--                     Event timestamp (JoyAxisEvent (JoyAxisEventData id axis value)) -> do
--                         modifyIORef' waitAxis (\_ -> False)
--                         putStrLn $ "What do you want to assign to axis " ++ (show axis) ++ " on controller " ++ (show id)
--                         putStrLn   "c : CC. $1 : CC number. put a ' to make it 14bit. [12'], [v12] $2 (opt): lower bound , [v0]. $3 (opt) : upper bound "
--                         putStrLn   "i : Inner state. $1 : name of the state"
--                         putStrLn   "p : Pitch Bend."
--                         putStrLn   "anything else : Nothing."
--                         getLine >>= \case
--                           'c':'\'':rest -> do
--                             -- parseArguments :: String -> Maybe (MidiCC, Int, Int)
--                               let arguments = parseArguments (rest)
--                               arguments >?>/ (putStrLn "Invalid arguments.") $ \((CC cc), lowerbound, upperbound) -> do
--                                   addBinding aMappings (fromIntegral id, fromIntegral axis) $ SM (CC14 cc) lowerbound upperbound
--                                   print (CC14 cc) 
--                                   putStrLn $ "(14 bits) assigned to axis" ++ (show axis)
--                           'i':' ':name -> do
--                             -- parseArguments :: String -> Maybe (MidiCC, Int, Int)
--                               addBinding aMappings (fromIntegral id, fromIntegral axis) $ SI name
--                               putStr $ "variable " ++ (show name)
--                               putStrLn $ " assigned to axis" ++ (show axis)
--                           'c':rest -> do
--                             -- parseArguments :: String -> Maybe (MidiCC, Int, Int)
--                               let arguments = parseArguments (rest)
--                               arguments >?>/ (putStrLn "Invalid arguments.") $ \(midiCC, lowerbound, upperbound) -> do
--                                   addBinding aMappings (fromIntegral id, fromIntegral axis) $ SM midiCC lowerbound upperbound
--                                   print midiCC
--                                   putStrLn $ " assigned to axis" ++ (show axis)
--                           _ -> putStrLn "..."
--                     _ -> return ()

--             let inputLoop = do
--                   putStrLn "Joystick detected. What do you want to do ?"
--                   putStrLn "z : assign action to reactive button"
--                   putStrLn "x : assign meaning to reactive axis"
--                   putStrLn "a n: assign meaning to axis n "
--                   putStrLn "r n : assign meaning to ball n"
--                   putStrLn "q : exit -configuration and start MIDI part"                
--                   command <- getLine
--                   case command of 
--                       "z" ->  do 
--                           assignButton
--                           inputLoop
--                       "x" ->  do 
--                           assignAxis
--                           inputLoop
--                       "q" -> putStrLn "standard joystick configuration ended."
--                       _ -> do
--                           putStrLn "invalid command"
--                           inputLoop
--             inputLoop
--             putStrLn "Configuration finished..."
            
    
-- loopParallax pr = do
  
--   let mappings = (midiMappings pr) 
--   let bMappings = buttonMs mappings 
--   let aMappings = axMs mappings 
--   let blMappings = blMs mappings
  
--   newM <-tryTakeMVar (newMappings pr)

--   newMidi <- newM >?>/ (return $ midiMappings pr) $ \newMapps -> do
--     print "New Mappings loaded."
--     return newMapps
--   events <- pollEvents  
--   forM_ events $ \event -> case event of 
--     Event timestamp (JoyButtonEvent (JoyButtonEventData id button JoyButtonPressed)) -> do
--       actions <- getBindings bMappings (fromIntegral id, fromIntegral button) 
--       forM_ actions (executeAction' pr True)
--     Event timestamp (JoyButtonEvent (JoyButtonEventData id button JoyButtonReleased)) -> do
--       actions <- getBindings  bMappings (fromIntegral id, fromIntegral button)
--       forM_ actions (executeAction' pr False)
--     Event timestamp (JoyAxisEvent (JoyAxisEventData id axis value)) -> do
--       meanings <- getBindings aMappings (fromIntegral id, fromIntegral axis) 
--       forM_ meanings(axisChange' pr value)
--     _ -> return ()
--   loopParallax pr {midiMappings=newMidi}
  

-- executeAction' pr  a b = do
--   print a
--   print b 
--   executeAction pr a b


-- -- executeAction parallax False (MapMod mappings) = --later
-- executeAction pr True (HardCC (CC cc) val _) = do
--   sendMessage (output pr) $ VS.fromList [0xb0, fromIntegral cc, fromIntegral val]
-- executeAction pr False (HardCC (CC cc) _ val) = do
--   sendMessage (output pr) $ VS.fromList [0xb0, fromIntegral cc, fromIntegral val]
-- executeAction pr True Panic = do
--   sendMessage (output pr) $ VS.fromList [123, 0]
-- -- executeAction pr True (Interval {interval = iv,  intervalId = iid, reversible = True}) = do
-- --   interval <- H.lookup (intervals pr) intervalId
-- --   interval >?> \iv' -> do
-- --     let newiv = iv'*iv
-- --     print newiv
-- --     H.insert (intervals pr) iid newiv
  
-- executeAction pr True (ToggleNote velId note) = do
--   vel <- H.lookup (sliders pr) velId  
--   vel >?>= 127 $ \vel -> do
--     let ratio :: Double =  (fromIntegral vel +  32768) /  (32768*2)  
--     let truevel = truncate $ ratio*127
--     sendMessage (output pr) $ VS.fromList [0b10010000, fromIntegral note, fromIntegral truevel]
-- executeAction pr False (ToggleNote velId note) = do
--   vel <- H.lookup (sliders pr) velId  
--   vel >?>= 0 $ \vel -> do
--     let ratio :: Double =  (fromIntegral vel +  312768) /  (32768*2)  
--     let truevel = truncate $ ratio*127
--     sendMessage (output pr) $ VS.fromList [0b10000000, fromIntegral note, fromIntegral vel]
-- -- executeAction pr False (ToggleNode velId interval baseId) = do
-- --   vel <- H.lookup (sliders pr) velId  
-- --   vel >?>= 0 $ \vel -> do
-- --     let ratio :: Double =  (fromIntegral vel +  32768) /  (32768*2)  
-- --     let truevel = truncate $ ratio*127
-- --     sendMessage (output pr) $ VS.fromList [0b10000000, fromIntegral note, fromIntegral truevel]
-- -- executeAction pr True (ToggleNode velId interval baseId) = do
-- --   vel <- H.lookup (sliders pr) velId  
-- --   vel >?>= 0 $ \vel -> do
-- --     let ratio :: Double =  (fromIntegral vel +  32768) /  (32768*2)  
-- --     let truevel = truncate $ ratio*127

-- --     sendMessage (output pr) $ VS.fromList [0b10010000, fromIntegral note, fromIntegral truevel]

-- executeAction pr _ _ = putStrLn "action not handled"

-- axisChange' pr a b = do
--   print a
--   print b 
--   axisChange pr a b

-- axisChange pr value (SI slider )  = do
--   H.insert (sliders pr) slider value
--   return ()


-- axisChange pr value (SM (CC cc) l u)  = do
--   let ratio :: Double =  (fromIntegral value +  32768) /  (32768*2)
--   let val = truncate $ ratio*((fromIntegral  $ u-l) + fromIntegral  l)*127
--   sendMessage (output pr) $ VS.fromList [0xb0, fromIntegral cc, fromIntegral val]

-- axisChange pr value (SM (CC14 cc)l u )  = do
--   -- 14 bit precision
--   let ratio :: Double = (fromIntegral value +  32768) /  (32768)
--   let a = f14to2x7 $ truncate $ ratio*((fromIntegral  $ u-l) + fromIntegral  l)*16383
--   a >?>/ (putStrLn "incorrect cc value.. wtf") $ \ (lsb,msb) -> do
--     (sendMessage (output pr) $ VS.fromList [0b10110000, fromIntegral cc, fromIntegral msb])
--     (sendMessage (output pr) $ VS.fromList [0b10110000, fromIntegral cc+32, fromIntegral lsb])


-- axisChange pr value (SP )  = do
--   let ratio :: Double = (fromIntegral value +  32768) /  (32768)
--   let a = f14to2x7 $ truncate $ ratio*16383
--   a >?>/ (putStrLn "incorrect pitchbend value.. wtf") $ \ (lsb,msb) -> do
--     sendMessage (output pr) $ VS.fromList [0b11100000, fromIntegral lsb, fromIntegral msb]


-- axisChange pr _ _  = return ()

