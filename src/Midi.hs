module Midi where



import GHC.Generics

import Control.Concurrent
import Control.Monad


import Text.Read
import Data.Char
import Debug.Trace
import Data.Bits
import Numeric

import Tools

noteval 'a'  = 0

noteval 'b'  = 2

noteval 'c'  = 3 - 12

noteval 'd'  = 5 - 12

noteval 'e'  = 7 - 12

noteval 'f'  = 8 - 12

noteval 'g'  = 10 - 12


data MidiCC = CC Int
            | CC14 Int
            | PB
            deriving (Eq,Show,Generic)

valid :: String -> Bool
valid "" = False
valid note  =   length note < 4 && length note > 1 
            &&  readMaybe @Int [head $ reverse note] /= Nothing
            &&  (toLower $ note !! 0)  `elem` ['a'..'g']
            &&  if length note == 3 then  
                    (note !! 1)  `elem` ['h','b','#']
                else True

mkNote :: String -> Maybe Int
mkNote note 
    | valid note =  Just $ 21 + octave*12 + val 
    | otherwise = Nothing
    where
        octave = read @Int [noteOct]
        rev = (reverse note)
        noteOct = rev !! 0
        noteI = reverse (drop 1 rev)
        notename = toLower $ noteI !! 0
        val
          | length noteI > 1 = 
              if (noteI !! 1) `elem` ['h','#'] then
                  noteval notename + 1
              else
                  noteval notename - 1
          | otherwise = noteval notename
            
f14to2x7 :: Int -> Maybe (Int,Int)
f14to2x7 x 
    | x < 2^14 = Just (lsb,msb)
    | otherwise = Nothing
    where 
        (lsb, msb) =  (read @Int $ drop 7 binliteral, read @Int $ take 7 binliteral)
        rawstring = showIntAtBase 2 intToDigit x ""
        binliteral = trim rawstring
        trim str@(x:str')
            | length str == 14 = str
            | otherwise = trim ('0':str)


parseArguments :: String -> Maybe (MidiCC, Int, Int)
parseArguments x = do
  let w = words x
  rawCC <- w !!? 0
  ccNum <- readMaybe @Int rawCC
  check1 <-  w !!? 1
  lower <- case head check1 of
      'v' -> do  
          let rawnum = (drop 1 check1)
          readMaybe @Int rawnum
      _ ->  return 0
  upper <- case head check1 of
      '^' -> do  
          let rawnum = (drop 1 check1)
          readMaybe @Int rawnum
      _ ->  return 127
  return (CC ccNum, lower,upper)

