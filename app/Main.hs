module Main where

import Lib
import App
import Parallax

import Types

main :: IO ()
main = do
    runPR start parallax0
    return ()

parallax0 :: Parallax
parallax0 = undefined