module App where


import GHC.Generics


-- i dont want to suffer 
import Data.Maybe
import PyF
import Control.Monad.IO.Class
import System.IO
import System.Environment
import Data.IORef
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Control.Concurrent
import Control.Monad
import Control.Lens.Combinators


import Device.Nintendo.Switch as Switch

import SDL.Input.Keyboard.Codes
import SDL.Input.Joystick as Joy
import SDL.Input.Keyboard as Keyb
import SDL.Init
import SDL.Event as SE
import SDL.Raw.Event (flushEvents)

-- midi

import Sound.RtMidi as Midi
import Sound.RtMidi.Report


-- conversion etc

import Foreign.C.Types
import Data.Int
import Text.Read
import qualified Data.ByteString as B

-- containers

import qualified Data.Text as T
import qualified Data.IntMap as IMap
import qualified Data.Vector as V
import qualified Data.Vector.Storable as VS
import qualified Data.HashTable.IO as H
import qualified Data.HashMap.Strict as DHS

-- bit fun  
import Data.Bits


--save/load
-- import Data.Serialize
import Data.Aeson 
        (encode
        ,encodeFile
        ,decode
        ,decodeFileStrict
        ,ToJSON
        ,FromJSON)

-- parsing
import Options.Applicative

import Tools
import Midi
import Types
import Parallax

instance Freezable Mappings where
  type Frozen Mappings = Mappings'
  freeze = frMappings
  thaw = thMappings
 

instance Freezable MidiMappings where
  type Frozen MidiMappings = MidiMappings'
  freeze = frMMappings
  thaw = thMMappings


instance Freezable JoystickMappings where
  type Frozen JoystickMappings = JoystickMappings'
  freeze = frJMappings
  thaw = thJMappings

instance Mappable Mappings where
  type MEnv Mappings = Parallax
  mapTypes = 
    ["Joystick"
    ,"Midi"]
  getAssignFuncs pr map  = return 
    [ joystickConfig
    , midiConfig
    ]
    where
      midiConfig pr map = 
        startAssigning (mMappings map) pr
      joystickConfig pr map =  
        startAssigning (jMappings map) pr
       

instance Mappable JoystickMappings where
  type MEnv JoystickMappings = Parallax
  mapTypes = 
    ["Axises"
    ,"Buttons"
    ,"Hats"]
  startAssigning pr map = do
        putStrLn "Joysticks configuration. What do you want to do ?"
        putStrLn "z : assign button"
        putStrLn "x : assign axis"
        putStrLn "b : assign balls"
        putStrLn "q : exit configuration"  
        [ buttonsConfig, axisConfig, hatsConfig] <- getAssignFuncs pr map 
        command <- getLine
        case command of 
            "z" ->  do 
              buttonsConfig pr map
            "x" ->  do 
              axisConfig pr map
            "q" -> putStrLn "Quitting configuration"
  getAssignFuncs = \ _ _ -> return 
    [ buttonsConfig
    , axisConfig
    , hatsConfig
    ]
    where
      axisConfig _ _ = do
        pollEvents
        putStrLn $ "Move an axis to start configuring it..."
        let startConfig id axis = do
              putStrLn $ [fmt|Starting configuration of Axis {axis}, joystick {id}|]
              putStrLn   "t : threshold actions"
              putStrLn   "v : Add to Inner variable. eg v x "
              putStrLn   "p : Pitch Bend."
              putStrLn   "anything else : Nothing."
        let wait 0  =  putStrLn "No Axis detected"
            wait x = do
                putStr "."
                event <- pollEvent
                case event of 
                  Just (Event timestamp (JoyAxisEvent (JoyAxisEventData id axis value))) -> do
                    startConfig id axis
                  _ -> do
                    hFlush stdout
                    threadDelay 10000
                    wait $ x - 1
        
        wait 100
      buttonsConfig = undefined
      hatsConfig = undefined

instance Mappable MidiMappings where
  type MEnv MidiMappings = Parallax
  mapTypes = 
    ["CC"
    ,"CC14"
    ,""]
  getMappingsFuncs = \ _ _ -> return 
    [ ccConfig
    , cc14Config
    ]
    where
      ccConfig = undefined
      cc14Config = undefined
      hatsConfig = undefined



instance  ParallaxFam ISettings  MLReport CLReport Parallax Mappings ParallaxM  where
  getCore =  PM $ \env -> do
    return (env,env)
  setCore new =  PM $ \env -> do
    return ((),new)
  modCore mod =  PM $ \env -> do
    env' <- mod env
    return ((),env')
  constructInit strings = return $ IS 2 2
  cleanParallax = do
    p <- getCore
    getInputs >>= mapM_ (liftIO. closePort)
    getOutputs >>= mapM_ (liftIO. closePort)
    (getConsole >>=) (>?> (liftIO. Switch.exit)) 
    getJoysticks >>=  mapM_ (liftIO.(closeJoystick.joy))
  getApi = fmap api getCore
  getOutputs = fmap (V.toList.outputs) getCore
  getInputs = fmap (V.toList.inputs) getCore
  getConsole = fmap console getCore
  getJoysticks = fmap joysticks getCore
  getMappings = fmap mappings getCore
  initParallax (IS inC outC) = PM $ \env -> do
    SDL.Init.initialize [InitJoystick,InitHaptic,InitGameController]
    console <- Switch.init
    apis <- Midi.compiledApis
    let api = if elem JackApi apis then
                JackApi
            else if elem AlsaApi apis then
                AlsaApi
            else
                DummyApi
    inputs <- forM [1..inC] $ \_ -> createInput  @IO api "ParallaxIn" 512
    outputs <- forM [1..outC] $ \_ -> createOutput @IO api "ParallaxOut"

    new <- newEmptyMVar
    
    bm <- H.new
    al <- H.new
    bl <- H.new
     

    cm <- H.new
    c14m <- H.new
   
    s <- H.new
    i <- H.new
    b <- H.new

    pxJoys  <- mapM mkPxJoy =<< availableJoysticks 
    
    
    let pr = Parallax {
      inputs = V.fromList inputs,
      outputs =  V.fromList outputs,
      api = api,
      console = Just console,
      joysticks = pxJoys,
      mappings = M {jMappings = JM bm al bl
                   ,mMappings = MM cm c14m},
      newMappings = new,
      intervals = i,
      bases = b,
      sliders= s
      
      }
    return ((),pr)
  config = do
    configMappings 


type TheParallax = ParallaxFam ISettings  MLReport CLReport Mappings Parallax 

type PX a =  forall m. TheParallax m => m a